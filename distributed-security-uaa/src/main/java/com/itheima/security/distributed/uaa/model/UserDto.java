package com.itheima.security.distributed.uaa.model;

import lombok.Data;

/**
 * @author 董文登
 * @date 2020/3/3
 */
@Data
public class UserDto {
    private String id;
    private String username;
    private String password;
    private String fullname;
    private String mobile;
}
