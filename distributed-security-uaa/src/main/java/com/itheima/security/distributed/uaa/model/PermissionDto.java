package com.itheima.security.distributed.uaa.model;

import lombok.Data;

/**
 * @author 董文登
 * @date 2020/3/10
 */
@Data
public class PermissionDto {
    private String id;
    private String code;
    private String description;
    private String url;
}
